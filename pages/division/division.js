import { br, defined, DEBUG, makeListItem, stringEscape, removeElement, focusTab } from '/modules/util.mjs';

// TODO: save open/close state of other divs
// TODO: only populate other div lists ontoggle if not already opened since last update

let supervizedTabs = [];
let storedTabs = [];
let name;
let label;
let thisId;
let thisWindowId;
let thisIndex;
let themeCache;
let openOtherDivsPrior = new Set();
let openOtherDivsLater = new Set();

let theme_css = document.getElementById('theme_css');
let name_form = document.getElementById('name_form');
let supervised_list = document.getElementById('supervised_list');
let stored_list = document.getElementById('stored_list');
let store_tabs_btn = document.getElementById('store_tabs_btn');
let restore_tabs_btn = document.getElementById('restore_tabs_btn');
let prior_division_list = document.getElementById('prior_division_list');
let later_division_list = document.getElementById('later_division_list');

supervised_list.onclick = clickTabList;
stored_list.onclick = clickStoredList;


let title_span = document.createElement('span');
title_span.classList.add('title_span');

let close_icon = document.createElement('span');
close_icon.classList.add('close_icon', 'hover_icon');
close_icon.innerHTML = '\u2715';

let prior_focus_icon = document.createElement('span');
prior_focus_icon.classList.add('prior_focus_icon', 'focus_icon', 'hover_icon');
prior_focus_icon.innerHTML = '<';

let later_focus_icon = document.createElement('span');
later_focus_icon.classList.add('later_focus_icon', 'focus_icon', 'hover_icon');
later_focus_icon.innerHTML = '>';

let icon_container = document.createElement('span');
icon_container.classList.add('flex_container', 'flex_between');

let tab_list = document.createElement('ul');
tab_list.classList.add('tab_list');
tab_list.setAttribute('empty_string', 'No tabs');

window.onbeforeunload = (event) =>
{
  if(storedTabs.length)
    chrome.runtime.sendMessage({saveBeforeUnload: storedTabs});
}

name_form.onsubmit = (event) =>
{
  if(saveName())
    name_form.name_input.blur();

  event.preventDefault();
  return false;
};

name_form.name_input.onblur = () =>
{
  saveName();
};

chrome.runtime.onMessage.addListener(function(msg, sender, reply)
{
  if(defined(msg.update))
  {
    supervizedTabs = msg.supervizedTabs;
    if(msg.divisionData) storedTabs = msg.divisionData;
    thisId = msg.update;
    thisWindowId = msg.windowId;
    thisIndex = msg.index;
    name = msg.name;
    update(msg.theme, msg.label, msg.priorDivisions, msg.laterDivisions);
  }
  else if(msg.movedTab)
  {// movedTab: {tabId, movedInfo, swappedDivs, you};
    // we know we didn't swap with another division, that's handled in background.js
    let leftAffectedList, rightAffectedList;
    if(msg.movedTab.swappedDivs[0] === thisId || msg.movedTab.swappedDivs[1] === thisId)
    {// this division moved
      rightAffectedList = supervised_list;

      if(prior_division_list.lastChild && openOtherDivsPrior.has(prior_division_list.lastChild.id))
        leftAffectedList = prior_division_list.lastChild.lastChild.lastChild;
    }
    else
    {
      rightAffectedList = document.getElementById(msg.movedTab.swappedDivs[0]);
      if(!rightAffectedList) rightAffectedList = document.getElementById(msg.movedTab.swappedDivs[1]);
      if(rightAffectedList)
      {
        if(rightAffectedList.previousSibling)
        {
          if(openOtherDivsPrior.has(rightAffectedList.previousSibling.id))
            leftAffectedList = rightAffectedList.previousSibling;
            if(openOtherDivsPrior.has(leftAffectedList) || openOtherDivsLater.has(leftAffectedList))
              leftAffectedList = leftAffectedList.lastChild.lastChild;
            else
              leftAffectedList = null;
        }
        else
          leftAffectedList = supervised_list;

        if(openOtherDivsPrior.has(rightAffectedList) || openOtherDivsLater.has(rightAffectedList))
          rightAffectedList = rightAffectedList.lastChild.lastChild;
        else
          rightAffectedList = null;
      }
    }
    // at this point right/left AffectedLists should,
    // point to affected lists or be null
    let movedRight = msg.movedTab.movedInfo.fromIndex < msg.movedTab.movedInfo.toIndex;
    let otherIndex;

    if(thisIndex === msg.movedTab.movedInfo.fromIndex)
    {
      thisIndex = msg.movedTab.movedInfo.toIndex;
      otherIndex = msg.movedTab.movedInfo.fromIndex;
    }
    else
    {
      thisIndex = msg.movedTab.movedInfo.fromIndex;
      otherIndex = msg.movedTab.movedInfo.toIndex;
    }

    if(leftAffectedList)
    {
      if(rightAffectedList)
      {// we have both lists
        if(movedRight)
          leftAffectedList.append(rightAffectedList.firstChild);
        else
          rightAffectedList.prepend(leftAffectedList.lastChild);
        return;
      }
      else
      {// only leftAffectedList
        if(movedRight)
          chrome.tabs.query({currentWindow: true, index: msg.movedInfo.fromIndex}, tabs =>
          {
            leftAffectedList.prepend(makeTabElement(tabs[0]));
          });
        else
          removeElement(leftAffectedList.lastChild);
        return;
      }
    }
    else
    {
      if(rightAffectedList)
      {// only rightAffectedList
        if(movedRight)
          removeElement(rightAffectedList.firstChild);
        else
          chrome.tabs.query({currentWindow: true, index: msg.movedTab.movedInfo.fromIndex}, tabs =>
          {
            rightAffectedList.prepend(makeTabElement(tabs[0]));
          });
        return;
      }
      else
      {// no divider moved
        if(prior_division_list.childNodes.length)
        {
          if(msg.movedInfo.fromIndex < prior_division_list.firstChild.lastChild.index)
            return;
        }
        else if(msg.movedInfo.fromIndex < thisIndex)
          return;
        else
        {
          chrome.runtime.sendMessage(
          {
            getAffectedDivision:
            {
              index: reply.movedInfo.fromIndex < reply.movedInfo.toIndex ?
                reply.movedInfo.fromIndex : reply.movedInfo.toIndex,
              windowId: movedInfo.windowId
            }
          }, reply =>
          {
            //reply.affectedId
            //reply.returned
            let onlyAffectedList;
            if(reply.affectedId === thisId)
              onlyAffectedList = supervised_list;
            else if(openOtherDivsPrior.has(affectedId) || openOtherDivsLater.has(affectedId))
              onlyAffectedList = document.getElementById(affectedId);

            if(onlyAffectedList) onlyAffectedList.insertBefore
            (
              onlyAffectedList.childNodes[reply.returned.index].nextSibling,
              onlyAffectedList.childNodes[reply.returned.index]
            );
          });
        }
      }
    }
  }
  else if(msg.newTab)
  {// messageCurrentActiveDiv({newTab});
    if(thisIndex < msg.newTab.index &&
      (!later_division_list.firstChild || (msg.newTab.index <= later_division_list.firstChild.index)))
    {//new tab must be added to supervised_list
      insertTabWithin(msg.newTab, supervised_list, thisIndex);
    }
    else
    {
      chrome.runtime.sendMessage({getAffectedDivision:
        {
          index: msg.newTab.index,
          windowId: msg.newTab.windowId,
          tab: msg.newTab
        }}, reply =>
      {
        //reply.affectedId
        //reply.returned
        if(!(openOtherDivsPrior.has(affectedId) || openOtherDivsLater.has(affectedId))) return;
        let affectedDiv = document.getElementById(reply.affectedId);
        if(DEBUG) console.assert(affectedDiv, 'openOtherDivsX has id with no corresponding element');

        insertTabWithin(reply.returned.tab, affectedDiv, affectedDiv.index);
      });
    }
  }
  else if(msg.removedTab)
  {// removedTab: tabId;
    removeElement(document.getElementById(msg.removedTab));
  }
  else if(msg.partialUpdate)
  {
    if(msg.partialUpdate.theme)
      refreshTheme(msg.partialUpdate.theme);
    if(defined(msg.partialUpdate.label))
      refreshLabel(msg.partialUpdate.label);
  }
});

function update(theme, label, priorDivisions, laterDivisions)
{
  console.log('updating');
  name_form.name_input.value = name;
  refreshLabel(label)

  refreshTheme(theme);

  supervised_list.innerHTML = '';
  if(supervizedTabs.length)
  {
    let content, close;
    populateTabs(supervizedTabs, supervised_list);
    store_tabs_btn.disabled = false;
  }
  else
  {
    store_tabs_btn.disabled = true;
  }

  stored_list.innerHTML = '';
  if(storedTabs.length)
  {
    for (var [index, tab] of storedTabs.entries())
    {
      let content = title_span.cloneNode(true);
      content.innerHTML = getTabString(tab);

      let item = makeListItem(document, [content, close_icon.cloneNode(true)],
        {index, class: 'flex_container flex_between'});
      item.onmouseenter = mouseenterTabItem;
      item.onmouseleave = mouseleaveTabItem;
      stored_list.append(item);
    }
    restore_tabs_btn.disabled = false;
  }
  else
    restore_tabs_btn.disabled = true;

  populateOtherDivs(prior_division_list, priorDivisions, prior_focus_icon, openOtherDivsPrior);
  populateOtherDivs(later_division_list, laterDivisions, later_focus_icon, openOtherDivsLater);
}

store_tabs_btn.onclick = () =>
{
  chrome.tabs.remove(supervizedTabs.map(tab => {return tab.id}));
  let offset = storedTabs.length;
  storedTabs.push(...supervizedTabs);

  for (let i = 0; i < supervizedTabs.length; i++)
  {
    supervised_list.firstChild.index = i + offset;
    stored_list.append(supervised_list.firstChild);

  }
  supervizedTabs = [];
  // stored_list.append(...supervised_list.childNodes);
  store_tabs_btn.disabled = true;
  restore_tabs_btn.disabled = false;
}

restore_tabs_btn.onclick = () =>
{
  stored_list.innerHTML = '';
  let index = thisIndex + 1;
  let limit = 1000;
  let url;
  while (storedTabs.length)
  {
    url = storedTabs[0].url;
    chrome.tabs.create({index: index++, url, active: false});
    if(DEBUG && (0 > limit--))
    {
      console.log('restore_tabs_btn.onclick while exiting due to limit breach. ');
      console.log('storedTabs.length = ', storedTabs.length);
      break;
    }
    storedTabs.shift();
  }
  restore_tabs_btn.disabled = true;
  store_tabs_btn.disabled = false;
}

function saveName()
{
  if(name !== name_form.name_input.value)
  {
    name = name_form.name_input.value;
    chrome.runtime.sendMessage({saveName: name});
    return true;
  }
  return false;
}

function populateOtherDivs(list, divisions, focus_icon, openOtherDivs)
{
  list.innerHTML = '';
  let newOpenOtherDivs = new Set();
  for(var division of divisions)
  {
    let details = document.createElement('details');
    let summary = document.createElement('summary');
    let iconsSpan = icon_container.cloneNode();
    let tabList = tab_list.cloneNode();

    iconsSpan.append(focus_icon.cloneNode(true), close_icon.cloneNode(true))

    summary.classList.add('flex_container', 'flex_between');
    summary.onclick = clickOtherDiv;
    summary.onmouseenter = mouseenterSummary;
    summary.onmouseleave = mouseleaveSummary;
    summary.append(division.name, iconsSpan);

    tabList.onclick = clickTabList;

    // details.id = division.id;
    // details.index = division.index;
    details.ontoggle = populateDetails;
    details.append(summary, tabList);
    if(openOtherDivs.has(division.id))
    {
      newOpenOtherDivs.add(division.id);
      details.setAttribute('open', '');
    }

    list.append(makeListItem(document, [details], {class: 'other_division', 'id': division.id, 'index': division.index}));
  }
  openOtherDivs = newOpenOtherDivs;
}

function populateDetails(event)
{
  if(event.target.open)
  {
    // console.log('poplating open details');
    if(event.target.parentNode.parentNode.id === 'prior_division_list')
      openOtherDivsPrior.add(event.target.lastChild.id);
    else
      openOtherDivsLater.add(event.target.lastChild.id);
    event.target.lastChild.innerHTML = '';
    chrome.windows.get(thisWindowId, {populate: true}, window =>
    {
      chrome.runtime.sendMessage({getTabList:
      {
        startId: event.target.parentNode.id,
        window
      }}, tabList =>
      {
        populateTabs(tabList, event.target.lastChild);
      });
    });
  }
  else
  {
    if(event.target.parentNode.parentNode.id === 'prior_division_list')
      openOtherDivsPrior.delete(event.target.lastChild.id);
    else
      openOtherDivsLater.delete(event.target.lastChild.id);
  }
}

function populateTabs(tabList, list_element)
{
  for (var tab of tabList)
  {
    list_element.append(makeTabElement(tab));
  }
}

function makeTabElement(tab)
{
  let content;
  content = title_span.cloneNode(true);
  content.innerHTML = getTabString(tab);

  let item = makeListItem(document, [content, close_icon.cloneNode(true)],
    {id: tab.id, class: 'flex_container flex_between'});
  item.onmouseenter = mouseenterTabItem;
  item.onmouseleave = mouseleaveTabItem;
  return item;
}

function getTabString(tab)
{
  return stringEscape(tab.title || tab.url || tab.pendingUrl || 'loading...');
}

function insertTabWithin(tab, list, divIndex)
{
  if(list.childNodes.length && supervised_list.childNodes[tab.index - divIndex])
  {// new tab won't be last in list
    list.insertBefore
    (
      makeTabElement(tab),
      supervised_list.childNodes[tab.index - divIndex]
    );
  }
  else
    // new tab is last in list
    list.append(makeTabElement(tab));
}

function refreshTheme(theme)
{
  if(theme !== themeCache)
  {
    theme_css.setAttribute('href', '/style/themes/' + theme + '.css');
    themeCache = theme;
  }
}

function refreshLabel(label)
{
  document.title = label || name;
}

function clickOtherDiv(event)
{
  if(event.target.classList.contains('focus_icon'))
  {
    clickFocusable(event.target.parentNode.parentNode.parentNode.parentNode);
    event.preventDefault();
  }
  else if(event.target.classList.contains('close_icon'))
  {
    clickCloseable(event.target.parentNode.parentNode.parentNode.parentNode);
    // removeElement(event.target.parentNode.parentNode.parentNode.parentNode);
    event.preventDefault();
  }
}

function clickTabList(event)
{
  if(event.target.classList.contains('title_span'))
    clickFocusable(event.target.parentNode);
  else if(event.target.classList.contains('close_icon'))
  {
    clickCloseable(event.target.parentNode);
    // removeElement(event.target.parentNode);
  }
}

function clickFocusable(node)
{
  focusTab(Number(node.id));
}

function clickCloseable(node)
{
  chrome.tabs.remove(Number(node.id));
}

function clickStoredList(event)
{
  if(event.target.classList.contains('close_icon'))
  {
    storedTabs.splice(Number(event.target.parentNode.index), 1);
    // delete storedTabs[Number(event.target.parentNode.index)];
    removeElement(event.target.parentNode);
    // event.target.parentNode.parentNode.removeChild(event.target.parentNode);
  }
}

function mouseenterTabItem(event)
{
  event.target.setAttribute('show', '');
}

function mouseleaveTabItem(event)
{
  event.target.removeAttribute('show');
}

function mouseenterSummary(event)
{
  event.target.setAttribute('show', '');
}

function mouseleaveSummary(event)
{
  event.target.removeAttribute('show');
}
