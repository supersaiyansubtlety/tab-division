import { defined, getMergedObject } from '/modules/util.mjs';
import { checkNight, stringToTheme, string24hrToInt, intTo24hrString } from '/modules/options.mjs';

// TODO: make window with divisions mock-up that demos settings

let optionsCache;

let options_form = document.getElementById('options_form');
let optionInputs = options_form.elements;
let timesInputs = [optionInputs.day_time, optionInputs.night_time];
let timeLabels = [document.getElementById('night_label'), document.getElementById('day_label')];

let theme_css = document.getElementById('theme_css');
let themeCache;

let saved_confirm = document.getElementById('saved_confirm');
let invalid_warn = document.getElementById('invalid_warn');
let saveConfirmTimeout, invalidWarnTimeout;

chrome.runtime.onMessage.addListener((msg, sender, reply) =>
{
  if(msg.updateTheme && msg.updateTheme !== themeCache)
  {
    themeCache = msg.updateTheme;
    displayTheme();
  }
  else if(msg.optionsChanged)
  {
    optionsCache = getMergedObject(msg.optionsChanged, optionsCache);
    displayOptions();
  }
});

function onDisplay()
{
  if(optionsCache)
  {
    displayOptions();
  }
  else
  {//options haven't been cached yet
    chrome.runtime.sendMessage({getOptions: true}, reply =>
    {
      optionsCache = reply;
      themeCache = stringToTheme(optionsCache.theme_select, optionsCache.night_time, optionsCache.day_time);
      displayTheme();
      displayOptions();
    });
  }
}
document.body.onfocus = document.body.onpageshow = onDisplay;

function saveOptions()
{
  if(!validateForm()) { return; }
  saved_confirm.classList.add('active');
  invalid_warn.classList.remove('active');

  clearTimeout(saveConfirmTimeout);
  clearTimeout(invalidWarnTimeout);
  saveConfirmTimeout = setTimeout(() => { saved_confirm.classList.remove('active'); }, 3000);

  let newOptions = {};
  if(optionInputs.default_name.value !== "" && optionInputs.default_name.value !== optionsCache.default_name)
    newOptions.default_name = optionInputs.default_name.value;
  if(optionInputs.match_name.checked !== optionsCache.match_name)
    newOptions.match_name = optionInputs.match_name.checked;
  if(optionInputs.custom_label_text.value !== optionsCache.custom_label_text)
    newOptions.custom_label_text = optionInputs.custom_label_text.value;
  if(optionInputs.theme_select.value !== optionsCache.theme_select)
    newOptions.theme_select = optionInputs.theme_select.value;
  if(string24hrToInt(optionInputs.night_time.value) !== optionsCache.night_time)
    newOptions.night_time = string24hrToInt(optionInputs.night_time.value);
  if(string24hrToInt(optionInputs.day_time.value) !== optionsCache.day_time)
    newOptions.day_time = string24hrToInt(optionInputs.day_time.value);

  if(!Object.keys(newOptions).length) { return; }

  console.log('updating options');
  optionsCache = getMergedObject(newOptions, optionsCache);

  chrome.storage.sync.set(newOptions);
  chrome.runtime.sendMessage({ setOptions: newOptions });
}

function displayOptions()
{
  optionInputs.default_name.value = optionsCache.default_name;
  displayLabelOptions(optionsCache.match_name);
  optionInputs.custom_label_text.setAttribute("value", optionsCache.custom_label_text);
  optionInputs.theme_select.value = optionsCache.theme_select;
  enableTimeInputs(optionsCache.theme_select === "auto");
  optionInputs.night_time.value = intTo24hrString(optionsCache.night_time);
  optionInputs.day_time.value = intTo24hrString(optionsCache.day_time);
}

function displayTheme()
{
  theme_css.setAttribute('href', '/style/themes/' + themeCache + '.css');
}

function displayLabelOptions(match_name)
{
  if(match_name)
  {
    optionInputs.match_name.checked = true;
    optionInputs.match_name.parentNode.setAttribute('checked', '');
    optionInputs.custom_label.parentNode.removeAttribute('checked');
    optionInputs.custom_label_text.disabled = true;
  }
  else
  {
    optionInputs.custom_label.checked = true;
    optionInputs.custom_label.parentNode.setAttribute('checked', '');
    optionInputs.match_name.parentNode.removeAttribute('checked');
    optionInputs.custom_label_text.disabled = false;
  }
}

function textOnBlur(event)
{
  if(event.target.value !== optionsCache[event.target.id])
  {
    saveOptions();
  }
}

function enableTimeInputs(enable)
{
  timesInputs.forEach(input => { input.disabled = !enable; });
  timeLabels.forEach(label =>
  {
    if(enable) { label.removeAttribute('disabled'); }
    else { label.setAttribute('disabled', ''); }
  });
}

function radioOnClick()
{
  displayLabelOptions(optionInputs.match_name.checked);
  saveOptions();
}

function themeOnChange()
{
  enableTimeInputs(optionInputs.theme_select.value === "auto")
  saveAndSetTheme();
}

function saveAndSetTheme(event)
{
  saveOptions();
}

[optionInputs.default_name, optionInputs.custom_label_text].forEach(input => { input.onblur = textOnBlur });
optionInputs.theme_select.onchange = themeOnChange;
[optionInputs.match_name, optionInputs.custom_label].forEach(radio => { radio.onclick = radioOnClick });
timesInputs.forEach(input => { input.onchange = saveAndSetTheme });

options_form.addEventListener('submit', (event) =>
{
  saveOptions();
  document.activeElement.blur();
  event.preventDefault();
  return false;
});

function validateForm()
{
  // if( optionInputs.default_name.value === "" ||
  //                    (!timesInputs[0].value) ||
  //                    (!timesInputs[1].value)
  // )
  let invalidReasons = '';
  let multipleReasons = false;
  if( optionInputs.default_name.value === "" )
    invalidReasons += 'empty name';
  if((!timesInputs[0].value))
  {
    if(invalidReasons !== '')
    {
      invalidReasons += ', ';
      multipleReasons = true;
    }
    invalidReasons += "incomplete 'Night theme start' specified";
  }
  if((!timesInputs[1].value))
  {
    if(invalidReasons !== '')
    {
      invalidReasons += ', ';
      multipleReasons = true;
    }
    invalidReasons += "incomplete 'Day theme start' specified";
  }

  if(invalidReasons !== '')
  {
    invalid_warn.innerHTML = 'Invalid settings, settings not saved. Reason' + (multipleReasons ? 's' : '') + ': ' + invalidReasons;

    invalid_warn.classList.add('active');
    saved_confirm.classList.remove('active');

    clearTimeout(saveConfirmTimeout);
    clearTimeout(invalidWarnTimeout);
    invalidWarnTimeout = setTimeout(() => { invalid_warn.classList.remove('active'); }, 8000);
    return false;
  }
  return true;
}
