export let DEBUG = true;

export let minsIn24hr = 24 * 60;

export function defined(object)
{
  return object !== undefined;
}

export function getMergedObject(sourceObj, destObj)
{
  for(var [key, value] of Object.entries(sourceObj))
  {
    destObj[key] = value;
  }
  return destObj;
}

export function br(element)
{
  element.appendChild(document.createElement('br'));
}

export function stringEscape(string)
{
  return string.replace('&', '&amp').replace('<', '&lt').replace('>', '&gt');
}

export function makeListItem(doc, items, attributes = false, prepend = false)//, innerWrap = false)
{
  let insertion = doc.createElement('li');
  if(attributes)
  {
    for(var [key, value] of Object.entries(attributes))
    {
      insertion.setAttribute(key, value);
    }
  }
  insertion.append(...items);
  // prepend ? list.prepend(insertion) : list.append(insertion);
  return insertion;
}

export function removeElement(element)
{
  if(!element) return;
  element.parentNode.removeChild(element);
}

export function focusTab(tabId)
{
  chrome.tabs.update(tabId, {active: true});
}

export function getTime()
{
  let date = new Date();
  console.log('got time: ', date);
  return date.getHours() * 60 + date.getMinutes() + date.getSeconds()/60 + date.getMilliseconds()/60000;
}

// export function wait(waited, condition, waiter)
// {
//   setTimeout(() =>
//   {//quadratically increasing wait time,
//   //  to avoid repetitive calls during long total wait time
//     if(condition < 1)
//     {//all divisions in window have returned
//       waiter.dispatchEvent(new CustomEvent('waitOver'), {detail: msg.windowId})
//       return;
//     }
//     if(waited < 54)
//     {//f(x) = (x + 1)^2, f(54 + 1) = 3026
//       waited++;
//     }
//     wait(waited, condition, waiter);
//   }, waited * waited);
// }
