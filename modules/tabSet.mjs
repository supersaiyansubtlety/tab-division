import { defined, DEBUG } from '/modules/util.mjs';

export function tabSet()
{
  this.list = [];
  this.map = {};
  // this.find(key);
  // this.insert(tabData);
  // this.delete(tabData);
  // this.swap(key1, key2);
  // this.reSort(oldKey, newKey);
}

//tabData has format {name, label, tabIndex, tabId};

tabSet.prototype.find = function(key, left = 0, right = this.list.length - 1)
{
  let i = getI(left, right);

  while (true)
  {
    if(key < this.list[i].tabIndex)
    {// |~~~+|~~~~|, move left
      right = i;
      i = getI(left, right);
      if(right === i)
      {// item goes before current first item
        return {index: i, exists: false};
      }
    }
    else if(this.list[i + 1] && key > this.list[i + 1].tabIndex)
    {//  |~~~-|-~~~|, move right
      left = i + 1;
      i = getI(left, right);
    }
    else
    {//  |~~~-|+~~~|
    //  key == list[i] || key == list[i + 1] || list[i] < key < list[i + 1];
      if(key === this.list[i].tabIndex)
      {
        return {index: i, exists: true};
      }
      else if(this.list[i + 1] && key === this.list[i + 1].tabIndex)
      {
        return {index: i + 1, exists: true};
      }
      else
      {
        return {index: i + 1, exists: false};
      }
    }
  }
}

function getI(left, right)
{
  return Math.floor((left + right)/2);
}

function findTwo(sortedPairObj, key1, key2)
{
  let r1 = sortedPairObj.find(key1);
  let r2;
  if(key1 < key2)
  {
    r2 = sortedPairObj.find(key2, r1.index);
  }
  else
  {//key1 > key2
    r2 = sortedPairObj.find(key2, 0, r1.index);
  }
  // console.log('findTwo got: ', key1, ', ', key2, ', returning: ', i1, ', ', i2);
  return [r1, r2];
}

//tabData = {name, tabIndex, tabId};
function getListItem(tabData)
{
  return {tabIndex: tabData.tabIndex, tabId: tabData.tabId};
}

function getMapItem(tabData)
{
  return {tabInfo: tabData.tabInfo, tabIndex: tabData.tabIndex};
}

tabSet.prototype.insert = function(tabData)
{//if tabData is new, insert and return its index
  if(DEBUG) this.verify();
  if(this.list.length === 0)
  {//if empty list, just insert
    this.list[0] = getListItem(tabData);
    this.map[tabData.tabId] = getMapItem(tabData);
    return 0;
  }
  let result = this.find(tabData.tabIndex);
  if(DEBUG) this.verify();
  // if(result.exists)
  //{//already exists, shift those after it to the right and then insert
  // if(!(result.index < (this.list.length - 1)))
  {
    this.alterIndex(result.index, 1, true);
  }
  if(DEBUG) this.verify();
  //actually insert
  this.list.splice(result.index, 0, getListItem(tabData));
  this.map[tabData.tabId] = getMapItem(tabData);
  if(DEBUG) this.verify();
  return result.index;
}

tabSet.prototype.delete = function(key)
{
  if(DEBUG) this.verify();
  if(DEBUG) console.assert(this.list.length || console.trace(), 'Attempting to delete from empty list. ');
  let result = this.find(key);
  if(DEBUG) console.assert(result.exists || console.trace(), 'Attempting to delete un-listed item. ');
  if(DEBUG) console.assert(defined(this.map[this.list[result.index].tabId]) || console.trace(), 'Attempting to delete un-mapped item. ');

  delete this.map[this.list[result.index].tabId];
  this.list.splice(result.index, 1);
  if(this.list.length) this.alterIndex(result.index, -1, true);
  if(DEBUG) this.verify();
  return result.index;
}

tabSet.prototype.swap = function(key0, key1)
{
  if(DEBUG) this.verify();
  if(key0 === key1) { return; }

  let results = findTwo(this, key0, key1);
  if(results[0].exists)
  {
    if(results[1].exists)
    {//both exist, so swap
      let swapId = this.list[results[0].index].tabId;
      this.map[swapId].tabIndex = key1;
      this.list[results[0].index].tabId = this.list[results[1].index].tabId;

      this.map[this.list[results[1].index].tabId].tabIndex = key0;
      this.list[results[1].index].tabId = swapId;

      if(DEBUG) this.verify();
      return [this.list[results[0].index].tabId, this.list[results[1].index].tabId];
    }
    else
    {//only key0 exists, so update key0 to key1
      if(results[0].index === results[1].index)
      {//not moving, just update it in place
        this.list[results[0].index].tabIndex =
        this.map[this.list[results[0].index].tabId].tabIndex = key1;
      }
      else
      {//move to new sort location and update from key0 to key1
        let id = this.list[results[0].index].tabId;
        this.list.splice(results[1].index, 0, {tabIndex: key1, tabId: id});
        this.map[id].tabIndex = key1;
        //if key0 (old) comes after key1 (new), shift index right to account for new insertion
        this.list.splice(results[0].index + Number(key0 > key1), 1);
      }

      if(DEBUG) this.verify();
      return [this.list[results[0].index].tabId, null];
    }
  }
  else if(results[1].exists)
  {//only key1 exists, so update key1 to key0
    if(results[0].index === results[1].index)
    {//not moving, just update it in place
      this.list[results[1].index].tabIndex =
      this.map[this.list[results[1].index].tabId].tabIndex = key0;
    }
    else
    {
      let id = this.list[results[1].index].tabId;
      this.list.splice(results[0].index, 0, {tabIndex: key0, tabId: id});
      this.map[id].tabIndex = key0;
      //if key1 (old) comes after key0 (new), shift index right to account for new insertion
      this.list.splice(results[1].index + Number(key1 > key0), 1);
    }

    if(DEBUG) this.verify();
    return [null, this.list[results[1].index].tabId];
  }
  if(DEBUG) this.verify();
  return [null, null];
  //may do nothing if neither key0 nor key1 exists
}

tabSet.prototype.alterIndex = function(start, alteration, startIsFound = false)
{
  // if(DEBUG) console.log('list before alt: ', JSON.parse(JSON.stringify(this.list)));
  if(DEBUG) this.verify();
  let startInd = (startIsFound ? start : this.find(start).index);
  for(var i = startInd; i < this.list.length; i++)
  {
    this.list[i].tabIndex += alteration;
    this.map[this.list[i].tabId].tabIndex += alteration;
    if(DEBUG) console.assert(this.list[i].tabIndex === this.map[this.list[i].tabId].tabIndex ||
      console.trace(), 'Mismatched map+list indices after atlering index. ');
  }
  if(DEBUG) this.verify();
  // if(DEBUG) console.log('list after alt: ', JSON.parse(JSON.stringify(this.list)));
}

if(DEBUG)
{
  tabSet.prototype.verify = function(print = false)
  {
    let prev = false;
    for(var listItem of this.list)
    {
      if(prev !== false && prev >= listItem.tabIndex)
      {
        console.error('tabSet verification failed due to un-ordered list: ', JSON.parse(JSON.stringify(this.list)));
        return false;
      }
      else if(this.map[listItem.tabId].tabIndex !== listItem.tabIndex)
      {
        console.error('tabSet verification failed due to mis-matched map and list: ', JSON.parse(JSON.stringify(this)));
        return false;
      }
      prev = listItem.tabIndex;
    }
    if(print) console.log('tabSet verification succeeded!: ' + (JSON.stringify(this)));
    return true;
  }

  function tabSetTest()
  {
    let print = false;
    let pary = new tabSet();
    let unSortedTabs =
    [
      {tabIndex: 0, tabId: 'a'},
      {tabIndex: 8, tabId: 'i'},
      {tabIndex: 1, tabId: 'b'},
      {tabIndex: 7, tabId: 'h'},
      {tabIndex: 2, tabId: 'c'},
      {tabIndex: 6, tabId: 'g'},
      {tabIndex: 3, tabId: 'd'},
      {tabIndex: 5, tabId: 'f'},
      {tabIndex: 4, tabId: 'e'}
    ];

    let preSortedTabs = JSON.parse(JSON.stringify(unSortedTabs));
    preSortedTabs.sort((a, b) => { return a.tabIndex - b.tabIndex; });
    // [
    //   [0, "a"],
    //   [1, "b"],
    //   [2, "c"],
    //   [3, "d"],
    //   [4, "e"],
    //   [5, "f"],
    //   [6, "g"],
    //   [7, "h"],
    //   [8, "i"]
    // ];
    if(print) console.log('preSortedTabs: ', JSON.parse(JSON.stringify(preSortedTabs)));
    for (let tabData of unSortedTabs)
    {
      // console.log('tabData: ', tabData);
      pary.insert(tabData);
    }
    // pary.verify(print);
    if(print) console.log('pary.list: ', JSON.parse(JSON.stringify(pary.list)));

    let expected = [];
    function checkIsExpected(msg)
    {
      if(expected.length !== pary.list.length)
      {
        console.error('tabSetTest failed due to unexpected length after ', msg, '.\nExpected: ', expected, '\nGot: ', JSON.parse(JSON.stringify(pary.list)));
        return 'failed';
      }
      for (var i = 0; i < pary.list.length; i++)
      {
        if(!(expected[i].tabIndex === pary.list[i].tabIndex && expected[i].tabId === pary.list[i].tabId))
        {
          console.error('tabSetTest failed due to unexpected ', msg, ' results.\nExpected: ', JSON.parse(JSON.stringify(expected)), '\nGot: ', JSON.parse(JSON.stringify(pary.list)));
          return 'failed';
        }
      }
      return 'succeeded!';
    }

    for (let i = 0; i < preSortedTabs.length / 2; i++)
    {
      if(defined(preSortedTabs[2 * i + 1]))
      {
        let expectedItem = /*JSON.parse(JSON.stringify(*/preSortedTabs[2 * i + 1]/*))*/;
        expectedItem.tabIndex -= i + 1;
        expected.push(expectedItem);
      }
      let toDelete = preSortedTabs[2 * i].tabIndex - i;
      pary.delete(toDelete);
    }
    pary.verify(print);
    // console.log('preSortedTabs: ', JSON.parse(JSON.stringify(preSortedTabs)));


    (print ? (console.log('evens deleted: ', checkIsExpected('deletion'))) : checkIsExpected('deletion'));

    let S1a = 0, S1b = 3, S2a = 1, S2b = 2;
    expected =
    [
      {tabIndex: S1a, tabId: pary.list[pary.find(S1b).index].tabId},
      {tabIndex: S1b, tabId: pary.list[pary.find(S1a).index].tabId},
      {tabIndex: S2a, tabId: pary.list[pary.find(S2b).index].tabId},
      {tabIndex: S2b, tabId: pary.list[pary.find(S2a).index].tabId}
    ];
    expected.sort((a, b) => { return a.tabIndex - b.tabIndex; });

    pary.swap(S1a, S1b);
    pary.swap(S2a, S2b);

    pary.verify(print);

    (print ? (console.log('swapped (', S1a,',', S1b,'), (', S2a, ',', S2b, '): ', checkIsExpected('swapping'))) : checkIsExpected('swapping'));

    S1a = 1, S1b = 8, S2a = 3, S2b = 5;
    // expected =
    // [
    //   [S1b, pary.list[S1a][1]],
    //   [S1b, pary.list[S1a][1]],
    //   [S2a, pary.list[S2b][1]],
    //   [S2b, pary.list[S2a][1]]
    // ];

    pary.swap(S1a, S1b);
    pary.swap(S2a, S2b);

    (print ? (console.log('reSorted (', S1a,',', S1b,'), (', S2a, ',', S2b, '): ', pary.verify(print) ? 'verified!' : 'failed')) : pary.verify(print));
  }
  // tabSetTest();
}
