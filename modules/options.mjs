import { getTime } from '/modules/util.mjs';

export let defaultOptions =
{
  default_name: 'Division',
  match_name: false,
  custom_label_text: '/',
  theme_select: "auto",
  night_time: (12 + 6) * 60,
  day_time: 6 * 60
}

export function checkNight(beginning, end, time = getTime(), midnight = end < beginning)
{
  if(midnight)
  {// night_time includes midnight
    if(time > beginning || time < end)
      return true;
  }
  else
  {// night_time doesn't include midnight
    if(time > beginning && time < end)
      return true;
  }
  return false;
};

export function string24hrToInt(string)
{
  if(!string) { return; }
  let pair = string.split(':');
  return Number(pair[0]) * 60 + Number(pair[1]);
}

export function intTo24hrString(int)
{
  let minutes = int % 60;
  let hours = (int - minutes)/60;
  minutes = String(minutes);
  hours = String(hours);
  if (minutes.length < 2) { minutes = '0' + minutes; }
  if (hours.length < 2) { hours = '0' + hours; }
  return hours + ':' + minutes;
}

export function stringToTheme(string, night_time, day_time)
{
  if(string === "auto")
  {
    return checkNight(night_time, day_time) ? "dark" : "light";
  }
  else
  {
    return string;
  }
}
