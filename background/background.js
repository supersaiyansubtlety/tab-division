import { defined, DEBUG, minsIn24hr, getTime, getMergedObject } from '/modules/util.mjs';
import { checkNight, defaultOptions, stringToTheme } from '/modules/options.mjs';
import { tabSet } from '/modules/tabSet.mjs';

// TODO: track if dividers in background need updates and only update if needed
// TODO: always refer to a tab's index in a window as its position

const extensionURL = chrome.runtime.getURL('/');
const divisionURL = extensionURL + 'pages/division/division.html';
const options_pageURL = extensionURL + 'pages/options-page/options-page.html';

let optionsCache = defaultOptions;
let recentlyChangedOptions;
let divisions = {};
let floatingDivisionData = {};

// these sets are flat because they are options-based and thus not per-window
let cleanThemes = new Set();
let cleanLabels = new Set();
// TabData is by window
let cleanTabData = {};

let themeTimeout;
let themeCache;
let darkCache;

chrome.runtime.onStartup.addListener(() =>
{
  chrome.storage.sync.get(defaultOptions, options => { setupOptions(options); });
});// onInstalled

// TODO: 
// chrome.runtime.onSuspend.addListener(function callback)

function setupOptions(options)
{
  optionsCache = getMergedObject(options, optionsCache);

  if(defined(options.match_name) || (!optionsCache.match_name && options.custom_label_text))
  {
    cleanLabels.clear();

    chrome.tabs.query({ active: true, url: divisionURL }, tabs =>
    {
      tabs.forEach(tab =>
      {
        chrome.tabs.sendMessage(tab.id, { partialUpdate: {label: (optionsCache.match_name ? false : optionsCache.custom_label_text) }});
        cleanLabels.add(tab.id);
      });
    });
  }

  if(options.theme_select)
  {
    if(options.theme_select === 'auto')
      // refreshAutoTheme checks if theme is actually automatic
      refreshAutoTheme();
    else
    {
      themeCache = optionsCache.theme_select;
      clearTimeout(themeTimeout);
      updateActiveExtensionTabThemes();
    }
  }
  else if (options.night_time || options.day_time)
    // auto theme time changed
    // refreshAutoTheme checks if theme is actually automatic
    refreshAutoTheme();
}// setupOptions

function setThemeTimeout(time, midnightIsDark = optionsCache.day_time < optionsCache.night_time)
{
  clearTimeout(themeTimeout);

  let timeout;

  if(midnightIsDark)
  {// dark_time includes midnightIsDark
    if(darkCache)
    {
      if(time > optionsCache.night_time)
        timeout = (minsIn24hr - time) + optionsCache.day_time;
      else
        timeout = optionsCache.day_time - time;
    }
    else
      timeout = optionsCache.night_time - time;
  }
  else
  {
    if(darkCache)
      timeout = optionsCache.day_time - time;
    else
    {
      if(time > optionsCache.day_time)
        timeout = (minsIn24hr - time) + optionsCache.night_time;
      else
        timeout = optionsCache.night_time - time;
    }
  }
  console.log('timing out for', timeout, 'minutes');

  //60000 b/c min->millisecond
  themeTimeout = setTimeout(() =>
  {
    darkCache = !darkCache;
    themeCache = (darkCache ? 'dark' : 'light');
    updateActiveExtensionTabThemes();
    setThemeTimeout(getTime());
  }, timeout * 60000);
}// setThemeTimeout

function refreshAutoTheme()
{
  if (optionsCache.theme_select !== 'auto') return;

  let time = getTime();
  let midnightIsDark = optionsCache.day_time < optionsCache.night_time;

  darkCache = checkNight(optionsCache.night_time, optionsCache.day_time, time, midnightIsDark);
  let oldThemeCache = themeCache;
  themeCache = (darkCache ? 'dark' : 'light');

  if(oldThemeCache !==  themeCache)
    updateActiveExtensionTabThemes();

  cleanThemes.clear();
  setThemeTimeout(time, midnightIsDark);
}

/*divisions {} format:
{
  windowIdA:
  {
    list:
    [
    [0]{
        tabIndex: indexA1,
        tabId: divisionIdA1
      },
    [1]{
        tabIndex: indexA2,
        tabId: divisionIdA2
      }
    ]
    //(indexA1 < indexA2)
    map
    {
      divisionIdA1:
      {
        tabIndex: indexA1,
        tabInfo:
        {
          name: nameA1
          label: labelA1
        }
      },
      divisionIdA2:
      {
        tabIndex: indexA2,
        tabInfo:
        {
          name: nameA2
          label: labelA2
        }
      }
    }
  },

  windowIdB:
  {
    list:
    [
    [0]{
        tabIndex: indexB1,
        tabId: divisionIdB1
      }
    ]
    //(indexB1 < indexB2)
    map
    {
      divisionIdB1:
      {
        tabIndex: indexB1,
        tabInfo:
        {
          name: nameB1
          label: labelB1
        }
      }
    }
  }
}*/
chrome.runtime.onMessage.addListener((msg, sender, reply) =>
{
  if(msg.saveBeforeUnload)
    floatingDivisionData[sender.tab.id] = msg.saveBeforeUnload;
  else if(msg.saveName)
  {
    divisions[sender.tab.windowId].map[sender.tab.id].tabInfo.name = msg.saveName;
    cleanTabData[sender.tab.windowId].clear();
  }
  else if(msg.getOptions)
  {
    reply(optionsCache);
    return true;
  }
  else if(msg.setOptions)
  {
    recentlyChangedOptions = msg.setOptions;
    setupOptions(msg.setOptions);
    chrome.tabs.query({ active: true, url: options_pageURL }, options_tabs =>
    {
      options_tabs.forEach(tab =>
      {
        chrome.tabs.sendMessage(tab.id, { optionsChanged: recentlyChangedOptions });
      });
    });
  }
  else if (msg.getTabList)
  {//[tab.title], {id: tab.id, index: tab.index
    let find = divisions[sender.tab.windowId].find(divisions[sender.tab.windowId].map[msg.getTabList.startId].tabIndex);
    let finish = divisions[sender.tab.windowId].list[find.index + 1];
    finish = (defined(finish) ? finish.tabIndex : msg.getTabList.window.tabs.length);

    reply(msg.getTabList.window.tabs.slice
    (
      divisions[sender.tab.windowId].map[msg.getTabList.startId].tabIndex + 1, //start
      finish //finish
    ));
    return true;
  }
  else if(msg.getAffectedDivision)
  {// getAffectedDivision: {index, windowId};
    reply(
    {
      affectedId: divisions[msg.getAffectedDivision.windowId].list
      [divisions[msg.getAffectedDivision.windowId].find(msg.getAffectedDivision.index).index - 1].tabId,
      returned: msg.getAffectedDivision
    });
  }
});// onMessage

chrome.browserAction.onClicked.addListener(function(tab)
{
  chrome.tabs.create({url: "/pages/division/division.html", index: tab.index});
});// onClicked

chrome.tabs.onReplaced.addListener(function(addedTabId, removedTabId)
{//tab replaced, usually when pre-rendered tab becomes standard tab
  console.log('onReplaced');
  chrome.tabs.get(addedTabId, (tab) =>
  {
    trackDivision(addedTabId, tab.windowId, tab.index, divisions[tab.windowId].map[removedTabId]);
    unTrackDivision(removedTabId, tab.windowId);

    if(DEBUG) verifyWindow(tab.windowId);
    // refreshAutoTheme checks if theme is actually automatic
    refreshAutoTheme();
  });
});// onTabReplaced

chrome.tabs.onActivated.addListener(function(activatedInfo)
{
  if(DEBUG) verifyWindow(activatedInfo.windowId);
  updateDivision(activatedInfo.tabId, activatedInfo.windowId, false);
});// onActivated

chrome.tabs.onMoved.addListener(function (tabId, movedInfo)
{
  if (divisions[movedInfo.windowId])
  {//tab moved in window that has divisions
    if(DEBUG) verifyWindow(movedInfo.windowId);
    cleanTabData[movedInfo.windowId].clear();
    // swap handles whether or not 0, 1, or 2 divisions were affected by the move
    let swappedDivs = divisions[movedInfo.windowId].swap(movedInfo.fromIndex, movedInfo.toIndex);
    applyToCurrentActiveDiv(tab =>
    {
      if(Math.abs(movedInfo.fromIndex - movedInfo.toIndex) > 1 ||
          ((tab.id === swappedDivs[0] || tab.id === swappedDivs[1]) &&
          swappedDivs[0] && swappedDivs[1]))
        // active div swapped positions with another div,
        // or move was not by one (probably a wrap-around move)
        updateDivision(tab.id, tab.windowId);
      else
      {
        chrome.tabs.sendMessage(tab.id, {movedTab: {tabId, movedInfo, swappedDivs, you: tab}});
        cleanTabData[movedInfo.windowId].add(tabId);
      }
    });
    if(DEBUG) verifyWindow(movedInfo.windowId);
  }
});// onMoved

chrome.tabs.onCreated.addListener(function (newTab)
{
  if(DEBUG && divisions[newTab.windowId]) divisions[newTab.windowId].verify();

  if((newTab.url || newTab.pendingUrl) === divisionURL)
  {//new division tab
    //insert here, onUpdated will update it
    if(!divisions[newTab.windowId])
      divisions[newTab.windowId] = new tabSet();

    if(cleanTabData[newTab.windowId]) cleanTabData[newTab.windowId].clear();

    divisions[newTab.windowId].insert({tabIndex: newTab.index, tabId: newTab.id, tabInfo: {}});
  }
  else if (defined(divisions[newTab.windowId]))
  {//not a division tab,
  //  but new tab is in a window that has divisions
    if(DEBUG) verifyWindow(newTab.windowId);
    divisions[newTab.windowId].alterIndex(newTab.index, 1);
    // update foreground division tab of this window,
    // does nothing if there isn't one
    cleanTabData[newTab.windowId].clear();
    messageCurrentActiveDiv({ newTab });

    if(DEBUG) verifyWindow(newTab.windowId);
  }

  if(DEBUG && divisions[newTab.windowId]) divisions[newTab.windowId].verify();
});// onCreated

chrome.tabs.onRemoved.addListener(function (tabId, removedInfo)
{//on tab close
  if (removedInfo.isWindowClosing)
  {//whole window is closing, untrack it
    delete divisions[removedInfo.windowId];
    delete cleanTabData[newTab.windowId];
  }
  else
  {//the entire window isn't closing
    if(!unTrackDivision(tabId, removedInfo.windowId))
    {//no division was untracked
      if(divisions[removedInfo.windowId])
      {//non-division was removed from window with divisions
        //can't get index of removed tabs,
        //so check every division tab in window until
        //one with incorrect index is found
        cleanTabData[removedInfo.windowId].clear();
        messageCurrentActiveDiv({removedTab: tabId});

        function checkNextDiv(curInd)
        {
          let curDiv = divisions[removedInfo.windowId].list[curInd];
          chrome.tabs.get(curDiv.tabId, tab =>
          {
            if(curDiv.tabIndex !== tab.index)
            {
              divisions[removedInfo.windowId].alterIndex(curDiv.tabIndex, -1);
              //must update here because of sync issues with onActivated
              // updateQueriedDivisions({active: true, windowId: removedInfo.windowId}, false);
              if(DEBUG) verifyWindow(removedInfo.windowId);
            }
            else if(++curInd < divisions[removedInfo.windowId].list.length)
              //if there's another
              checkNextDiv(curInd);
            // else removedTab must have been right of right-most division
            else
              if(DEBUG) verifyWindow(removedInfo.windowId);
          });
        }
        checkNextDiv(0);
      }
    }
    //if division was untracked, unTrackDivision updated indices
  }
});// onRemoved

chrome.tabs.onUpdated.addListener(function(tabId, changeInfo, tab)
{
  if(changeInfo.status === 'complete')
  {
    if(DEBUG) verifyWindow(tab.windowId);
    // delete clean status to force update
    if(cleanTabData[tab.windowId]) cleanTabData[tab.windowId].delete(tabId);
    updateDivision(tabId, tab.windowId);
  }
});// onUpdated

chrome.tabs.onDetached.addListener(function (tabId, detachedInfo)
{//on tab close
  if(!unTrackDivision(tabId, detachedInfo.oldWindowId))
  {//no division was untracked
    if(divisions[detachedInfo.oldWindowId])
    {//non-division was detached from a window with divisions
      cleanTabData[detachedInfo.oldWindowId].clear();
      divisions[detachedInfo.oldWindowId].alterIndex(detachedInfo.oldPosition, -1);
      if(DEBUG) verifyWindow(detachedInfo.oldWindowId);
    }
  }
  //if a division was untracked, unTrackDivision updated indices
});// onDetached

chrome.tabs.onAttached.addListener(function(tabId, attachedInfo)
{
  chrome.tabs.get(tabId, function(tab)
  {
    if((tab.url || tab.pendingUrl) === divisionURL)
      //the attached tab is a division
      trackDivision(tabId, attachedInfo.newWindowId, tab.index, tab.title);
    else if(divisions[attachedInfo.newWindowId])
    {//non-division attached to window with divisions
      divisions[attachedInfo.newWindowId].alterIndex(attachedInfo.newPosition, 1);
      cleanTabData[attachedInfo.newWindowId].clear();
      if(DEBUG) verifyWindow(attachedInfo.newWindowId);
    }
    //else non-division tab attached to window with no divisions,
    //do nothing
  });
});// onAttached

function trackDivision(tabId, windowId, index, tabInfo = {})
{
  if(DEBUG) verifyWindow(windowId);

  if(!divisions[windowId])
  {
    divisions[windowId] = new tabSet();
    cleanTabData[windowId] = new Set();
  }
  else
    cleanTabData[windowId].clear();

  divisions[windowId].insert({ tabIndex: index, tabId, tabInfo });
  updateDivision(tabId, windowId);

  if(DEBUG) verifyWindow(windowId);
}

function unTrackDivision(tabId, windowId)
{
  if (defined(divisions[windowId]) && defined(divisions[windowId].map[tabId]))
  {//if it was a division tab that closed
    divisions[windowId].delete(divisions[windowId].map[tabId].tabIndex);
    cleanTabData[windowId].clear();
    // if(DEBUG) verifyWindow(windowId);
    if(divisions[windowId].list.length < 1)
    {//divisions[removedInfo.windowId] is empty
      delete divisions[windowId];
      delete cleanTabData[windowId];
    }
    return true;//true: a division was untracked
  }
  return false;//false: no division was untracked
}// unTrackDivision

function makeUpdateMsg(tabId, window)
{
  let tabIndex = divisions[window.id].map[tabId].tabIndex;
  let listPos = divisions[window.id].find(tabIndex).index;

  let nextDivision = divisions[window.id].list[listPos + 1] ;
  nextDivision = (defined(nextDivision) ? nextDivision.tabIndex : false);

  let supervizedTabs = window.tabs.slice
  (
    //start immediately after this tab
    tabIndex + 1,
    //finish before next division'sindex or last tab
    (nextDivision !== false ? nextDivision : window.tabs.length)
  );

  let priorDivisions = getOtherDivArray(0, listPos, divisions[window.id]);

  let laterDivisions = getOtherDivArray(listPos + 1,
    divisions[window.id].list.length, divisions[window.id]);

  let ret =
  {
    update: tabId,
    windowId: window.id,
    index: tabIndex,
    supervizedTabs,
    priorDivisions,
    laterDivisions,
    theme: themeCache,
    name: divisions[window.id].map[tabId].tabInfo.name || optionsCache.default_name,
    label: (optionsCache.match_name ? false : optionsCache.custom_label_text)
  };

  if(floatingDivisionData[tabId])
  {
    ret.divisionData = floatingDivisionData[tabId];
    delete floatingDivisionData[tabId];
  }
  return ret;
  // TODO: track number of supervized tabs
}// makeUpdateMsg

function updateDivision(divisionId, windowId, verify = true)
{
  if (divisions[windowId] && defined(divisions[windowId].map[divisionId]))
  {//it's a division tab
    if(DEBUG && verify) verifyWindow(windowId);

    let doUpdate = false;
    if(!cleanTabData[windowId])
    {
      cleanTabData[windowId] = new Set();
      doUpdate = true;
    }
    else if(!cleanTabData[windowId].has(divisionId))
      doUpdate = true;

    if(doUpdate)
    {
      cleanTabData[windowId].add(divisionId);
      cleanThemes.add(divisionId);
      cleanLabels.add(divisionId);
      chrome.windows.get(windowId, { populate: true }, function(window)
      {
        chrome.tabs.sendMessage(divisionId, makeUpdateMsg(divisionId, window));
      });
    }
    else
    {
      let partialUpdate = {};
      if(!cleanThemes.has(divisionId))
      {
        partialUpdate.theme = themeCache;
        cleanThemes.add(divisionId);
        doUpdate = true;
      }
      if(!cleanLabels.has(divisionId))
      {
        partialUpdate.label = (optionsCache.match_name ? false : optionsCache.custom_label_text);
        cleanLabels.add(divisionId);
        doUpdate = true;
      }
      if(doUpdate)
      {
        chrome.tabs.sendMessage(divisionId, partialUpdate);
      }
    }
  }
}// updateDivision

function updateActiveExtensionTabThemes()
{
  chrome.tabs.query({ active: true, url: options_pageURL }, tabs =>
  {
    tabs.forEach(tab =>
    {
      chrome.tabs.sendMessage(tab.id, { updateTheme: themeCache });
    });
  });

  chrome.tabs.query({ active: true, url: divisionURL }, tabs =>
  {
    tabs.forEach(tab =>
    {
      chrome.tabs.sendMessage(tab.id, { partialUpdate: { theme: themeCache } });
      cleanThemes.add(tab.id);
    });
  });
}

function getOtherDivArray(i, end, divWindow)
{
  let array = [];
  let curId;
  for (; i < end; i++)
  {
    curId = divWindow.list[i].tabId;
    array.push({id: curId, name: divWindow.map[curId].tabInfo.name || optionsCache.default_name, tabIndex: divWindow.list[i].tabIndex});
  }
  return array;
}

function messageCurrentActiveDiv(msg)
{
  applyToCurrentActiveDiv(tab =>
  {
    chrome.tabs.sendMessage(tab.id, msg);
    cleanTabData[tab.windowId].add(tab.id);
  });
}

function applyToCurrentActiveDiv(fxn)
{
  chrome.tabs.query({ active: true, currentWindow: true, url: divisionURL }, tabs =>
  {
    if(!tabs.length) return;
    fxn(tabs[0]);
    cleanTabData[tabs[0].windowId].add(tabs[0].id);
  });
}

function test()
{
  var trace = new Error();
  Error.captureStackTrace(trace);
}

let verifyWindow;
if(DEBUG)
{
  verifyWindow = (windowId) =>
  {
    if(!divisions[windowId]) return;
    divisions[windowId].verify();

    var trace = new Error();
    Error.captureStackTrace(trace);
    chrome.windows.get(windowId, {populate: true, windowTypes: ['normal']}, window =>
    {
      for(var tab of window.tabs)
      {
        console.assert((!defined(divisions[windowId].map[tab.id])) ||
          (tab.index === divisions[windowId].map[tab.id].tabIndex) ||
          !new Notification('division index discrepancy found!'),
          'division index discrepancy found. \nstack trace: ' + trace.stack +
          '\nstored: ' + String(divisions[windowId].map[tab.id] ? divisions[windowId].map[tab.id].tabIndex : '') +
          ', actual: ' + String(tab ? tab.index : ''));
      }
    });
  }
}
